/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer''
|
*/

import Route from '@ioc:Adonis/Core/Route'
import Product from 'App/Models/Product'

Route.get('/', async ({ view }) => {
  return view.render('welcome')
})

Route.get('/product', async (ctx) => {
  let products = await Product.all()
  return ctx.response.json(products)
})

Route.post('/product', async(ctx) => {
  let {name, price, qty} = ctx.request.body()
  const product = await Product.create({name, price, qty})

  return ctx.response.status(201).json(product)
})

Route.patch('/product/:id', async(ctx) => {
  let {name, price, qty} = ctx.request.body()

  const product = await Product.findOrFail(ctx.params.id)

  product.name = name ?? product.name
  product.price = price ?? product.price
  product.qty = qty ?? product.qty

  await product.save()

  return ctx.response.status(200).json(product)
})

Route.delete('/product/:id', async(ctx) => {
  const product = await Product.findOrFail(ctx.params.id)
  await product.delete()

  return ctx.response.status(200).json({message: 'successfully delete product'})
})
