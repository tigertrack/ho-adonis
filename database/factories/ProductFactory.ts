import product from 'App/Models/product'
import Factory from '@ioc:Adonis/Lucid/Factory'

export default Factory.define(product, ({ faker }) => {
  return {
    name: faker.name.fullName({gender: 'male'}),
    price: faker.datatype.number({min: 25000, max: 100000}),
    qty: faker.datatype.number({min: 1, max: 999}),
  }
}).build()
