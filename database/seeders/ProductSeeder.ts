import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Product from 'App/Models/Product'
import { ProductFactory } from 'Database/factories'

export default class extends BaseSeeder {
  public async run () {
    const productDummies = await ProductFactory.createMany(10)
    await Product.createMany(productDummies)
  }
}
